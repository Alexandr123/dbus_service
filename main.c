#include <stdio.h>
#include "calcService/calcService.h"

int main(int argc, char *argv[]) {
    sd_bus_slot *slot = NULL;
    sd_bus *bus = NULL;
    int r;
    struct calcProp cp;

    cp.last_result = 0;
    calcService_init(&bus, &slot, &cp);

    while(1) {
        calcService_run(&bus, &slot);
    }
}