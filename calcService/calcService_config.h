#include <stddef.h>
//================================================================================================================================================================================
// Calculator object
//================================================================================================================================================================================
#define CALC_SERVICE_OBJ_PATH               "/net/poettering/Calculator"
#define CALC_SERVICE_INTERFACE_NAME         "net.poettering.Calculator"

// Signals
#define CALC_SERVICE_METHOD_INVIKED_S   "MethodInvoked"
// Methods
int method_multiply(sd_bus_message *m, void *userdata, sd_bus_error *ret_error);
int method_divide(sd_bus_message *m, void *userdata, sd_bus_error *ret_error);
// Properties
struct calcProp {
        int last_result;
};

//The vtable of object, implements the net.poettering.Calculator interface
static const sd_bus_vtable calculator_vtable[] = {
  SD_BUS_VTABLE_START(0),
  SD_BUS_METHOD("Multiply", "xx", "x", method_multiply, SD_BUS_VTABLE_UNPRIVILEGED),
  SD_BUS_METHOD("Divide",   "xx", "x", method_divide,   SD_BUS_VTABLE_UNPRIVILEGED),
  SD_BUS_SIGNAL(CALC_SERVICE_METHOD_INVIKED_S,"s",0),
  SD_BUS_WRITABLE_PROPERTY("LastResult", "u", NULL, NULL, offsetof(struct calcProp, last_result), 0),
  SD_BUS_VTABLE_END
};
//================================================================================================================================================================================