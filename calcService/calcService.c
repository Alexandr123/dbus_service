#include "calcService.h"
//================================================================================================================================================================================
// Service public functions
//================================================================================================================================================================================
/**
 * @brief Calculator service init.
 * 
 * @param bus - bus pointer
 * @param slot - slot pointer
 * @return int
 */
int calcService_init(sd_bus **bus, sd_bus_slot **slot, struct calcProp *cp) {
    int r;

    /* Connect to the user bus this time */
    r = sd_bus_open_user(bus);
    if (r < 0) {
        fprintf(stderr, "Failed to connect to system bus: %s\n", strerror(-r));
        sd_bus_slot_unref(*slot);
        sd_bus_unref(*bus);
        return 1;
    }

    /* Install the object */
    r = sd_bus_add_object_vtable(*bus,
                                slot,
                                CALC_SERVICE_OBJ_PATH,  /* object path */
                                CALC_SERVICE_INTERFACE_NAME,   /* interface name */
                                calculator_vtable,
                                cp);
    if (r < 0) {
        fprintf(stderr, "Failed to issue method call: %s\n", strerror(-r));
        sd_bus_slot_unref(*slot);
        sd_bus_unref(*bus);
        return 2;
    }

    /* Take a well-known service name so that clients can find us */
    r = sd_bus_request_name(*bus, CALC_SERVICE_INTERFACE_NAME, 0);
    if (r < 0) {
        fprintf(stderr, "Failed to acquire service name: %s\n", strerror(-r));
        sd_bus_slot_unref(*slot);
        sd_bus_unref(*bus);
        return 3;
    }

    bus = bus;
    slot = slot;
    return 0;
}
//================================================================================================================================================================================
/**
 * @brief Calculator service main routine.
 * 
 * @param bus - bus pointer
 * @param slot - slot pointer
 * @return int 
 */
int calcService_run(sd_bus **bus, sd_bus_slot **slot){
    int r;

    /* Process requests */
    r = sd_bus_process(*bus, NULL);
    if (r < 0) {
        fprintf(stderr, "Failed to process bus: %s\n", strerror(-r));
        sd_bus_slot_unref(*slot);
        sd_bus_unref(*bus);
        return r < 0 ? EXIT_FAILURE : EXIT_SUCCESS;
    }
    if (r > 0) /* we processed a request, try to process another one, right-away */
        return 0;

    /* Wait for the next request to process */
    r = sd_bus_wait(*bus, (uint64_t) -1);
    if (r < 0) {
        fprintf(stderr, "Failed to wait on bus: %s\n", strerror(-r));
        sd_bus_slot_unref(*slot);
        sd_bus_unref(*bus);
        return r < 0 ? EXIT_FAILURE : EXIT_SUCCESS;
    }
}
//================================================================================================================================================================================

//================================================================================================================================================================================
// D-Bus service  methods implementation
//================================================================================================================================================================================
/**
 * @brief D-Bus .Multiply method handler
 * 
 * @param m - D-Bus message object.
 * @param userdata - holder for propertys.
 * @param ret_error - holder for error message.
 * @return int 
 */
int method_multiply(sd_bus_message *m, void *userdata, sd_bus_error *ret_error) {
    int64_t x, y;
    int64_t result;
    int r;
    sd_bus_error error = SD_BUS_ERROR_NULL;
    const char *intf = sd_bus_message_get_interface(m),
            *path = sd_bus_message_get_path(m);
	sd_bus *bus = sd_bus_message_get_bus(m);
    struct calcProp *cp = userdata;

    /* Read the parameters */
    r = sd_bus_message_read(m, "xx", &x, &y);
    if (r < 0) {
        fprintf(stderr, "Failed to parse parameters: %s\n", strerror(-r));
        return r;
    }

    /* Emit signal */
    r = sd_bus_emit_signal(bus, path, intf, "MethodInvoked", "ss",
        "Echo multiply method was invoked", path);
    if (r < 0) {
        fprintf(stderr, "Failed to emit signal: %s\n", strerror(-r));
        return r;
    }

    /* Calculate, save and return result */
    result = x * y;
    cp->last_result = result;
    return sd_bus_reply_method_return(m, "x", result);
}
//================================================================================================================================================================================
/**
 * @brief D-Bus .Divide method handler
 * 
 * @param m - D-Bus message object.
 * @param userdata - holder for propertys.
 * @param ret_error - holder for error message.
 * @return int 
 */
int method_divide(sd_bus_message *m, void *userdata, sd_bus_error *ret_error) {
    int64_t x, y;
    int64_t result;
    int r;
    sd_bus_error error = SD_BUS_ERROR_NULL;
    const char *intf = sd_bus_message_get_interface(m),
            *path = sd_bus_message_get_path(m);
	sd_bus *bus = sd_bus_message_get_bus(m);
    struct calcProp *cp = userdata;

    /* Read the parameters */
    r = sd_bus_message_read(m, "xx", &x, &y);
    if (r < 0) {
        fprintf(stderr, "Failed to parse parameters: %s\n", strerror(-r));
        return r;
    }

    /* Emit signal */
    r = sd_bus_emit_signal(bus, path, intf, "MethodInvoked", "ss",
        "Echo divide method was invoked", path);
    if (r < 0) {
        fprintf(stderr, "Failed to emit signal: %s\n", strerror(-r));
        return r;
    }

    /* Return an error on division by zero */
    if (y == 0) {
        sd_bus_error_set_const(ret_error, "net.poettering.DivisionByZero", "Sorry, can't allow division by zero.");
        return -EINVAL;
    }

    /* Calculate, save and return result */
    result = x / y;
    cp->last_result = result;
    return sd_bus_reply_method_return(m, "x", result);
}
//================================================================================================================================================================================