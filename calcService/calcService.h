#include <stdlib.h>
#include <errno.h>
#include <systemd/sd-bus.h>
#include "calcService_config.h"

int calcService_init(sd_bus **bus, sd_bus_slot **slot, struct calcProp *cp);
int calcService_run(sd_bus **bus, sd_bus_slot **slot);

